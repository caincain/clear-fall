package constant

import fall "gitee.com/caincain/clear-fall"

var (
	BytesQN   = fall.StringToBytes("QN")
	BytesST   = fall.StringToBytes("ST")
	BytesCN   = fall.StringToBytes("CN")
	BytesPW   = fall.StringToBytes("PW")
	BytesMN   = fall.StringToBytes("MN")
	BytesFlag = fall.StringToBytes("Flag")
	BytesPNUM = fall.StringToBytes("PNUM")
	BytesPNO  = fall.StringToBytes("PNO")
	HjHeader  = fall.StringToBytes("##")
	HjFooter  = []byte{13, 10}
	DataSplit = fall.StringToBytes("CP=&&")
)
