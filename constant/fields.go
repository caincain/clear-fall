package constant

/*
	//case "RngMin":
	//
	//case "RngMax":
*/
const (
	Empty = ""

	SuffixCheckValue    = "-CheckValue"
	SuffixStandardValue = "-StandardValue"
	SuffixRngMin        = "-RngMin"
	SuffixRngMax        = "-RngMax"
	SuffixRtd           = "-Rtd"
	SuffixRT            = "-RT"
	SuffixRS            = "-RS"
	SuffixCou           = "-Cou"
	SuffixEFlag         = "-EFlag"
	SuffixFlag          = "-Flag"
	SuffixZsMax         = "-ZsMax"
	SuffixZsAvg         = "-ZsAvg"
	SuffixZsMin         = "-ZsMin"
	SuffixZsRtd         = "-ZsRtd"
	SuffixMax           = "-Max"
	SuffixAvg           = "-Avg"
	SuffixMin           = "-Min"
	SuffixData          = "-Data"
	SuffixDayData       = "-DayData"
	SuffixNightData     = "-NightData"
	SuffixInfo          = "-Info"
	SuffixSN            = "-SN"
	SuffixSampleTime    = "-SampleTime"
	SuffixLog           = "-Log"
)

const (
	QN   = "QN="
	ST   = "ST"
	CN   = "CN="
	PW   = "PW"
	MN   = "MN="
	Flag = "Flag"
)
const CHJ = "CHJ"
