package entity

type Msg struct {
	ChannelId string            `json:"channelId"`
	Content   []byte            `json:"content"`
	System    string            `json:"system"`
	Area      string            `json:"area"`
	NodeKey   string            `json:"nodeKey"`
	MsgType   string            `json:"msgType"`
	RecvTime  string            `json:"recvTime"`
	Metadata  map[string]string `json:"metadata,omitempty"`
}
