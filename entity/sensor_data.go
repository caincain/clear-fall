package entity

type SensorDataMsg struct {
	MetaData  map[string]string `json:"metadata"`
	Content   string            `json:"content"`
	Subsystem string            `json:"subsystem"`
	RecvTime  string            `json:"recvTime"`
}

type Data struct {
	CN    string `json:"cn"`
	ST    string `json:"st"`
	QN    string `json:"qn"`
	MN    string `json:"mn"`
	Dev   string `json:"dev,omitempty"`
	Usage string `json:"usage,omitempty"`
	PW    string `json:"pw"`
	Flag  string `json:"flag"`
	CP    CP     `json:"cp"`
}

type CP struct {
	Prot          *string             `json:"prot,omitempty"`
	Hver          *string             `json:"hver,omitempty"`
	SvDate        *string             `json:"svDate,omitempty"`
	DataTime      *string             `json:"dataTime,omitempty"`
	LoginTime     *string             `json:"loginTime,omitempty"`
	AlarmTime     *string             `json:"alarmTime,omitempty"`
	HeartbeatTime *int64              `json:"heartbeatTime,omitempty"`
	Flag          map[string]*string  `json:"flag,omitempty"`
	Rtd           map[string]*float64 `json:"rtd,omitempty"`
	Avg           map[string]*float64 `json:"avg,omitempty"`
	Min           map[string]*float64 `json:"min,omitempty"`
	Max           map[string]*float64 `json:"max,omitempty"`
	ZsMax         map[string]*float64 `json:"zsMax,omitempty"`
	ZsMin         map[string]*float64 `json:"zsMin,omitempty"`
	ZsAvg         map[string]*float64 `json:"ZsAvg,omitempty"`
	ZsRtd         map[string]*float64 `json:"zsRtd,omitempty"`
	Rs            map[string]*float64 `json:"rs,omitempty"`
	RngMax        map[string]*float64 `json:"rngMax,omitempty"`
	RngMin        map[string]*float64 `json:"rngMin,omitempty"`
	Others        *string             `json:"others,omitempty"`
}

type SensorData struct {
	MetaData map[string]string `json:"metadata"`
	Msg      SensorDataMsg     `json:"msg"`
	MsgData  Data              `json:"msgData"`
	Modified bool              `json:"modified"`
	Dirty    bool              `json:"dirty"`
	Invalid  bool              `json:"invalid"`
}
