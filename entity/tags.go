package entity

import (
	"bytes"
	"encoding/json"
	"fmt"
	fall "gitee.com/caincain/clear-fall"
	"gitee.com/caincain/clear-fall/constant"
	"gitee.com/caincain/common-brick/lotus"
	"strconv"
	"time"
)

type ChjMsgTags struct {
	header  *bytes.Buffer
	body    *bytes.Buffer
	Qn      string            `json:"qn"`
	St      string            `json:"st"`
	Cn      uint64            `json:"cn"`
	Pw      string            `json:"pw"`
	Mn      string            `json:"mn"`
	Flag    interface{}       `json:"flag"`
	PNum    interface{}       `json:"pNum"`
	PNo     interface{}       `json:"pNo"`
	Unknown map[string]string `json:"unknown"`
}

func NewChjMsgTags() *ChjMsgTags {
	return &ChjMsgTags{Qn: "", St: "", Cn: 0, Pw: "", Mn: "", Flag: nil, PNum: nil, PNo: nil, Unknown: nil}
}

func (o *ChjMsgTags) ToJsonBytes() (out []byte) {
	out, _ = json.Marshal(&o)
	return
}
func (o *ChjMsgTags) FromBytes(in []byte) (err error) {
	i := bytes.Index(in, []byte("CP=&&"))
	if i < 0 {
		return fall.MissingCP
	}
	tagBytes := in[6 : i-1]
	for _, pair := range bytes.Split(tagBytes, []byte{';'}) {
		i = bytes.IndexByte(pair, '=')
		if i < 0 {
			return fall.InvalidTags
		}
		k, v := pair[:i], pair[i+1:]
		switch true {
		case bytes.Equal(k, constant.BytesQN):
			o.Qn = string(v)
		case bytes.Equal(k, constant.BytesST):
			o.St = string(v)
		case bytes.Equal(k, constant.BytesCN):
			if o.Cn, err = fall.Bytes2Uint(v); err != nil {
				return err
			}
		case bytes.Equal(k, constant.BytesPW):
			o.Pw = string(v)
		case bytes.Equal(k, constant.BytesMN):
			o.Mn = string(v)
		case bytes.Equal(k, constant.BytesFlag):
			if o.Flag, err = fall.Bytes2Uint(v); err != nil {
				return err
			}
		case bytes.Equal(k, constant.BytesPNUM):
			if o.PNum, err = fall.Bytes2Uint(v); err != nil {
				return err
			}
		case bytes.Equal(k, constant.BytesPNO):
			if o.PNo, err = fall.Bytes2Uint(v); err != nil {
				return err
			}
		default:
			if o.Unknown == nil {
				o.Unknown = map[string]string{}
			}
			o.Unknown[string(k)] = string(v)
		}
	}
	return
}
func (o *ChjMsgTags) From(msg *Msg) (err error) {
	in := msg.Content
	return o.FromBytes(in)
}

func (o *ChjMsgTags) Is2017() bool {
	return o.Flag != nil && (o.Flag.(uint64)&4) == 4
}
func (o *ChjMsgTags) Is2005() bool {
	return o.Flag != nil && (o.Flag.(uint64)&4) != 4
}

func (o *ChjMsgTags) WrapChj2017() (out []byte) {
	if o.Flag != nil && (o.Flag.(uint64)&1) != 1 {
		return
	}
	o.body = bytes.NewBuffer(nil)
	switch o.Cn {
	case 2011, 2021, 2051, 2061, 2041, 2031, 2081, 3019, 3020:
		o.writeQN()
		o.writeST("91")
		o.writeCN("9014")
		o.writeOtherTags()
		o.writeFlag(o.Flag.(uint64) & 252)
		o.body.Write(fall.StringToBytes("CP=&&&&"))
		o.writeHeader()
		o.writeCrc()
		o.header.Write(o.body.Bytes())
		return o.header.Bytes()
	case 1013:
		o.writeQN()
		o.writeST("91")
		o.writeCN("9013")
		o.writeOtherTags()
		o.writeFlag(o.Flag.(uint64) & 252)
		o.body.Write(fall.StringToBytes("CP=&&"))
		o.writeCN("1012")
		o.writeSystemTime()
		o.body.Truncate(o.body.Len() - 1)
		o.body.Write(fall.StringToBytes("&&"))
		o.writeHeader()
		o.writeCrc()
		o.header.Write(o.body.Bytes())
		return o.header.Bytes()
	case 2091, 3005, 3016, 8051:
		o.body = bytes.NewBuffer(nil)
		o.writeQN()
		o.writeST("91")
		o.writeCN("9014")
		o.body.Write(fall.StringToBytes("CP=&&"))
		o.writeQN()
		o.writeCN(strconv.FormatUint(o.Cn, 10))
		o.body.Truncate(o.body.Len() - 1)
		o.body.Write(fall.StringToBytes("&&"))
		o.writeHeader()
		o.writeCrc()
		o.header.Write(o.body.Bytes())
		return o.header.Bytes()
	default:
		lotus.Debug(fmt.Sprintf("not implement with cn %d of WrapChj2017", o.Cn))
		return
	}
}
func (o *ChjMsgTags) WrapCChj2005() (out []byte) {
	if o.Flag != nil && (o.Flag.(uint64)&1) != 1 {
		return
	}
	if o.Cn == 8011 {
		//login
		o.body = bytes.NewBuffer(nil)
		o.writeQN()
		o.writeST(o.St)
		o.writeCN("8012")
		o.writeOtherTags()
		o.writeFlag(0)
		o.body.Write(fall.StringToBytes("CP=&&Login=1&&"))
		o.writeHeader()
		o.writeCrc()
		o.header.Write(o.body.Bytes())
		return o.header.Bytes()
	} else {
		if o.Flag != nil && (o.Flag.(uint64)&1) != 1 {
			return nil
		}
		o.body = bytes.NewBuffer(nil)
		o.writeQN()
		o.writeST(o.St)
		o.writeCN("9014")
		o.writeOtherTags()
		o.writeFlag(0)
		o.body.Write(fall.StringToBytes("CP=&&"))
		o.writeQN()
		o.writeCN(strconv.FormatUint(o.Cn, 10))
		o.body.Truncate(o.body.Len() - 1)
		o.body.Write(fall.StringToBytes("&&"))
		o.writeHeader()
		o.writeCrc()
		o.header.Write(o.body.Bytes())
		return o.header.Bytes()
	}
}
func (o *ChjMsgTags) WrapChj2005() (out []byte) {
	if o.Qn == "" {
		o.Qn = time.Now().Format("20060102150405")
	}
	if o.Cn == 8011 {
		//login
		o.body = bytes.NewBuffer(nil)
		o.writeQN()
		o.writeST("91")
		o.writeCN("8012")
		o.writeOtherTags()
		o.writeFlag(0)
		o.body.Write(fall.StringToBytes("CP=&&Login=1&&"))
		o.writeHeader()
		o.writeCrc()
		o.header.Write(o.body.Bytes())
		return o.header.Bytes()
	} else {
		if o.Flag != nil && (o.Flag.(uint64)&1) != 1 {
			return nil
		}
		o.body = bytes.NewBuffer(nil)
		o.writeST(o.St)
		o.writeCN("9014")
		o.writeOtherTags()
		o.writeFlag(0)
		o.body.Write(fall.StringToBytes("CP=&&"))
		o.writeQN()
		o.writeCN(strconv.FormatUint(o.Cn, 10))
		o.body.Truncate(o.body.Len() - 1)
		o.body.Write(fall.StringToBytes("&&"))
		o.writeHeader()
		o.writeCrc()
		o.header.Write(o.body.Bytes())
		return o.header.Bytes()
	}
}
func (o *ChjMsgTags) writeQN() {
	if o.Qn != "" {
		o.body.Write(constant.BytesQN)
		o.body.WriteByte('=')
		o.body.Write(fall.StringToBytes(o.Qn))
		o.body.WriteByte(';')
	}
}
func (o *ChjMsgTags) writeST(st string) {
	if st != "" {
		o.body.Write(constant.BytesST)
		o.body.WriteByte('=')
		o.body.Write(fall.StringToBytes(st))
		o.body.WriteByte(';')
	}
}
func (o *ChjMsgTags) writeFlag(flag uint64) {
	o.body.Write(constant.BytesFlag)
	o.body.WriteByte('=')
	o.body.Write(strconv.AppendUint([]byte{}, flag, 10))
	o.body.WriteByte(';')
}
func (o *ChjMsgTags) writeCN(cn string) {
	o.body.Write(constant.BytesCN)
	o.body.WriteByte('=')
	o.body.Write(fall.StringToBytes(cn))
	o.body.WriteByte(';')
}
func (o *ChjMsgTags) writeOtherTags() {
	if o.Pw != "" {
		o.body.Write(constant.BytesPW)
		o.body.WriteByte('=')
		o.body.Write(fall.StringToBytes(o.Pw))
		o.body.WriteByte(';')
	}
	if o.Mn != "" {
		o.body.Write(constant.BytesMN)
		o.body.WriteByte('=')
		o.body.Write(fall.StringToBytes(o.Mn))
		o.body.WriteByte(';')
	}
}
func (o *ChjMsgTags) writeCrc() {
	crc := strconv.AppendUint([]byte{}, uint64(fall.CheckSum(o.body.Bytes())), 16)
	for i := len(crc); i < 4; i++ {
		o.body.WriteByte('0')
	}
	o.body.Write(bytes.ToUpper(crc))
	o.body.Write(constant.HjFooter)
}
func (o *ChjMsgTags) writeSystemTime() {
	o.body.Write(fall.StringToBytes("SystemTime"))
	o.body.WriteByte('=')
	o.body.Write(fall.StringToBytes(time.Now().Format("20060102150405")))
	o.body.WriteByte(';')
}
func (o *ChjMsgTags) writeHeader() {
	o.header = bytes.NewBuffer(nil)
	body := o.body.Bytes()
	length := "0000" + strconv.Itoa(len(body))
	o.header.Write(constant.HjHeader)
	o.header.Write(fall.StringToBytes(length[len(length)-4:]))
}
