package entity

import (
	"gitee.com/caincain/common-brick/ginkgo"
	"strconv"
	"strings"
)

type ChjMsgDataTags struct {
	Qn      string            `json:"qn,omitempty"`
	St      string            `json:"st,omitempty"`
	Cn      int               `json:"cn,omitempty"`
	Pw      string            `json:"pw,omitempty"`
	Mn      string            `json:"mn,omitempty"`
	Dev     string            `json:"dev,omitempty"`
	Usage   string            `json:"usage,omitempty"`
	Flag    int               `json:"flag,omitempty"`
	PNum    int               `json:"pNum,omitempty"`
	PNo     int               `json:"pNo,omitempty"`
	Unknown map[string]string `json:"unknown,omitempty"`
}

type ChjMsgDataCmdParams struct {
	AlarmTime     *string             `json:"alarm_time,omitempty"`
	HeartbeatTime *int64              `json:"heartbeatTime,omitempty"`
	SVer          *string             `json:"sVer,omitempty"`
	HVer          *string             `json:"hVer,omitempty"`
	SvDate        *string             `json:"sVDate,omitempty"`
	Prot          *string             `json:"prot,omitempty"`
	LoginTime     *string             `json:"loginTime,omitempty"`
	SystemTime    *string             `json:"systemTime,omitempty"`
	QnRtn         *int                `json:"qnRtn,omitempty"`
	ExeRtn        *int                `json:"exeRtn,omitempty"`
	RtdInterval   *int                `json:"rtdInterval,omitempty"`
	MinInterval   *int                `json:"minInterval,omitempty"`
	RestartTime   *string             `json:"restartTime,omitempty"`
	SampleTime    map[string]string   `json:"sampleTime,omitempty"`
	RngMin        map[string]*float64 `json:"rngMin,omitempty"`
	RngMax        map[string]*float64 `json:"rngMax,omitempty"`
	Min           map[string]*float64 `json:"min,omitempty"`
	Avg           map[string]*float64 `json:"avg,omitempty"`
	Max           map[string]*float64 `json:"max,omitempty"`
	ZsRtd         map[string]*float64 `json:"zsRtd,omitempty"`
	ZsMin         map[string]*float64 `json:"zsMin,omitempty"`
	ZsAvg         map[string]*float64 `json:"zsAvg,omitempty"`
	ZsMax         map[string]*float64 `json:"zsMax,omitempty"`
	Flag          map[string]*string  `json:"flag,omitempty"`
	EFlag         map[string]*string  `json:"eFlag,omitempty"`
	Log           map[string]*string  `json:"Log,omitempty"`
	Cou           map[string]*float64 `json:"cou,omitempty"`
	RS            map[string]*float64 `json:"rs,omitempty"`
	RT            map[string]*float64 `json:"rt,omitempty"`
	Data          map[string]*float64 `json:"data,omitempty"`
	DayData       map[string]*float64 `json:"dayData,omitempty"`
	NightData     map[string]*float64 `json:"nightData,omitempty"`
	CheckValue    map[string]*float64 `json:"checkValue,omitempty"`
	StandardValue map[string]*float64 `json:"standardValue,omitempty"`
	PolId         *string             `json:"polId,omitempty"`
	BeginTime     *string             `json:"beginTime,omitempty"`
	EndTime       *string             `json:"endTime,omitempty"`
	NewPW         *string             `json:"newPW,omitempty"`
	OverTime      *int64              `json:"overTime,omitempty"`
	ReCount       *int64              `json:"reCount,omitempty"`
	VaseNo        *int64              `json:"vaseNo,omitempty"`
	CstartTime    *string             `json:"cstartTime,omitempty"`
	Ctime         *int64              `json:"cTime,omitempty"`
	Stime         *int64              `json:"sTime,omitempty"`
	Info          map[string]*string  `json:"info,omitempty"`
	InfoId        *string             `json:"infoId,omitempty"`
	Rtd           map[string]*float64 `json:"rtd,omitempty"`
	SN            map[string]*string  `json:"sn,omitempty"`
	Unknown       map[string]*string  `json:"unknown,omitempty"`
	DataTime      *string             `json:"dataTime,omitempty"`
}
type ChjMsgDataExceptionInfo struct {
	Key           string `json:"key,omitempty"`
	Message       string `json:"message,omitempty"`
	ExceptionType string `json:"exceptionType,omitempty"`
}
type ChjMsgData struct {
	Msg           *Msg                     `json:"msg,omitempty"`
	Tags          *ChjMsgDataTags          `json:"tags,omitempty"`
	CmdParams     *ChjMsgDataCmdParams     `json:"cmdParams,omitempty"`
	ExceptionInfo *ChjMsgDataExceptionInfo `json:"exceptionInfo,omitempty"`
	Metadata      map[string]string        `json:"metadata,omitempty"`
}

func (o *ChjMsgData) ToSensorData() *SensorData {
	return &SensorData{
		MetaData: o.Msg.Metadata,
		Msg: SensorDataMsg{
			MetaData:  o.Msg.Metadata,
			Content:   strings.TrimSpace(string(o.Msg.Content)),
			Subsystem: o.Msg.System,
			RecvTime:  o.Msg.RecvTime,
		},
		MsgData: Data{
			CN:    strconv.Itoa(o.Tags.Cn),
			ST:    o.Tags.St,
			QN:    o.Tags.Qn,
			MN:    o.Tags.Mn,
			PW:    o.Tags.Pw,
			Dev:   o.Tags.Dev,
			Usage: o.Tags.Usage,
			Flag:  strconv.Itoa(o.Tags.Flag),
			CP: CP{
				Prot:          o.CmdParams.Prot,
				Hver:          o.CmdParams.HVer,
				SvDate:        o.CmdParams.SvDate,
				DataTime:      o.CmdParams.DataTime,
				LoginTime:     o.CmdParams.LoginTime,
				AlarmTime:     o.CmdParams.AlarmTime,
				HeartbeatTime: o.CmdParams.HeartbeatTime,
				Flag:          o.CmdParams.Flag,
				Rtd:           o.CmdParams.Rtd,
				Avg:           o.CmdParams.Avg,
				Min:           o.CmdParams.Min,
				Max:           o.CmdParams.Max,
				ZsMax:         o.CmdParams.ZsMax,
				ZsMin:         o.CmdParams.ZsMin,
				ZsAvg:         o.CmdParams.ZsAvg,
				ZsRtd:         o.CmdParams.ZsRtd,
				Rs:            o.CmdParams.RS,
				RngMax:        o.CmdParams.RngMax,
				RngMin:        o.CmdParams.RngMin,
				Others: func(in map[string]*string) *string {
					if in == nil {
						return nil
					}
					str := ginkgo.ToJsonString(o.CmdParams.Unknown)
					return &str
				}(o.CmdParams.Unknown),
			},
		},
		Modified: false,
		Dirty:    false,
		Invalid:  o.ExceptionInfo == nil,
	}
}

/*
##0644QN=20230723144833376;ST=31;CN=2011;PW=300000;MN=35000014201D534000010001;Flag=7;
PNUM=2;PNO=1;CP=&&DataTime=20230723144830;CG11001-Rtd=0.000,CG11001-Flag=N;
CG11701-Rtd=0.000,CG11701-Flag=N;CG11801-Rtd=0.000,CG11801-Flag=N;CG11201-Rtd=0.000,CG11201-Flag=N;

*/
