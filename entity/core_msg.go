package entity

import "encoding/json"

type CoreMsg struct {
	Metadata  map[string]string `json:"metadata"`
	Content   string            `json:"content"`
	Subsystem string            `json:"subsystem"`
	RecvTime  string            `json:"recvTime"`
}

func (o *CoreMsg) ToJsonBytes() []byte {
	raw, _ := json.Marshal(&o)
	return raw
}
//{"metadata":{"kafkaInsertTime":"2021-11-24T09:56:04.415646","area":"FS"},"content":"##0260QN=20211124095600;ST=32;CN=2011;PW=123456;MN=44060121001002;Flag=1;CP=&&DataTime=20211124095600;PROT=CHJ212_V2_4;L01-Rtd=1.422;L01-Flag=N;011-Rtd=24.8;011-Flag=N;060-Rtd=2.67;060-Flag=A;101-Rtd=0.158;101-Flag=N;W01-Rtd=19.0;W01-Flag=N;W02-Rtd=0.25;W02-Flag=A&&1E00","subsystem":"WDP","recvTime":"2021-11-24T09:55:57.696422"}