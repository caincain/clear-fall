package clear_fall

import "errors"

var (
	WrongHeader      = errors.New("wrong header")
	WrongFooter      = errors.New("wrong footer")
	WrongLength      = errors.New("wrong length")
	WrongDataRegion  = errors.New("wrong data region")
	WrongPassword    = errors.New("wrong password")
	WrongCrc         = errors.New("wrong crc")
	MissingCP        = errors.New("CP is missing")
	InvalidTags      = errors.New("invalid tags")
	IgnoreValue      = errors.New("ignore value")
	WrongDigitOfTime = errors.New("time fields should be at least 14 digit")
)
