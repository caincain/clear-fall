package clear_fall

type Checker interface {
	New() Checker
	WithPassword(password string) Checker
	CheckHeader() (err error)
	CheckLength() (err error)
	CheckFooter() (err error)
	CheckCrc() (err error)
	CheckPassword() (err error)
	Check(raw []byte) (err error)
}
