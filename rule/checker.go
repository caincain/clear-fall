package rule

import (
	"bytes"
	"fmt"
	fall "gitee.com/caincain/clear-fall"
	"gitee.com/caincain/clear-fall/constant"
	"gitee.com/caincain/common-brick/lotus"
	"strconv"
	"unicode"
)

type Hj212CheckerImplement struct {
	pw     string
	raw    []byte
	length int
	crc    uint16
	isHan  bool
}

func (o *Hj212CheckerImplement) WithPassword(password string) fall.Checker {
	o.pw = password
	return o
}

func (o *Hj212CheckerImplement) New() fall.Checker {
	return &Hj212CheckerImplement{}
}

func NewHj212CheckerImplement() *Hj212CheckerImplement {
	return &Hj212CheckerImplement{}
}

func (o *Hj212CheckerImplement) Check(raw []byte) (err error) {
	if len(raw) < 12 {
		return fall.WrongLength
	}
	if !bytes.Contains(raw, constant.DataSplit) {
		return fall.WrongDataRegion
	}
	o.raw = raw
	o.isHan = o.containsChinese()
	if err = o.CheckHeader(); err != nil {
		return
	}
	if err = o.CheckLength(); err != nil {
		return
	}
	if err = o.CheckFooter(); err != nil {
		return
	}
	if err = o.CheckPassword(); err != nil {
		return
	}
	if err = o.CheckCrc(); err != nil {
		return
	}
	return
}

func (o *Hj212CheckerImplement) CheckHeader() (err error) {
	if !bytes.HasPrefix(o.raw, constant.HjHeader) {
		return fall.WrongHeader
	}
	if o.length, err = strconv.Atoi(string(o.raw[2:6])); err != nil {
		return
	}
	return
}
func (o *Hj212CheckerImplement) CheckFooter() (err error) {
	if !bytes.HasSuffix(o.raw, constant.HjFooter) {
		return fall.WrongFooter
	}
	return
}
func (o *Hj212CheckerImplement) CheckLength() (err error) {
	var u uint64
	if o.isHan {
		lotus.Debug(fmt.Sprintf("msg(%d) contains chinese and mismatch the length %d", len(o.raw), o.length))
		if u, err = strconv.ParseUint(string(o.raw[len(o.raw)-6:len(o.raw)-2]), 16, 64); err != nil {
			return
		}
		o.crc = uint16(u)
		return
	}
	if o.length+12 != len(o.raw) {
		return fall.WrongLength
	}
	if u, err = strconv.ParseUint(string(o.raw[o.length+6:o.length+10]), 16, 64); err != nil {
		return
	}
	o.crc = uint16(u)
	return
}

func (o *Hj212CheckerImplement) CheckPassword() (err error) {
	if o.pw != "" {
		pwIndex := bytes.Index(o.raw, constant.BytesPW)
		if string(o.raw[pwIndex+3:pwIndex+9]) != o.pw {
			return fall.WrongPassword
		}
	}
	return
}

func (o *Hj212CheckerImplement) CheckCrc() (err error) {
	if o.isHan {
		if fall.CheckSum(o.raw[6:len(o.raw)-6]) != o.crc {
			return fall.WrongCrc
		}
		return
	}
	if fall.CheckSum(o.raw[6:6+o.length]) != o.crc {
		return fall.WrongCrc
	}
	return
}

func (o *Hj212CheckerImplement) containsChinese() (flag bool) {
	for _, v := range string(o.raw) {
		if unicode.Is(unicode.Han, v) {
			flag = true
			return
		}
	}
	return
}
