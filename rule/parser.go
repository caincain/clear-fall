package rule

import (
	"bytes"
	"fmt"
	fall "gitee.com/caincain/clear-fall"
	"gitee.com/caincain/clear-fall/constant"
	"gitee.com/caincain/clear-fall/entity"
	"gitee.com/caincain/common-brick/lotus"
	"strconv"
	"strings"
	"time"
)

type ChjMsgUtils struct {
	msg *entity.Msg
	raw []byte
	cmd []byte
}

func (o *ChjMsgUtils) GetCmd() []byte { return o.cmd }

func NewChjMsgUtils(msg *entity.Msg) *ChjMsgUtils {
	return &ChjMsgUtils{msg: msg, raw: msg.Content}
}

func parseFloat(s string, bitSize int) (*float64, error) {
	if v, e := strconv.ParseFloat(s, bitSize); e == nil {
		return &v, nil
	} else {
		return nil, e
	}
}

func NewChjMsgUtilsFromCoreMsg(msg entity.CoreMsg) *ChjMsgUtils {
	raw := []byte(msg.Content)
	if !bytes.HasSuffix(raw, constant.HjFooter) {
		raw = append(raw, constant.HjFooter...)
	}
	return &ChjMsgUtils{
		msg: &entity.Msg{
			System:   msg.Subsystem,
			Area:     msg.Metadata["area"],
			RecvTime: msg.RecvTime,
		},
		raw: raw,
	}
}

func NewChjMsgUtilsFromBytes(in []byte) *ChjMsgUtils {
	if !bytes.HasSuffix(in, constant.HjFooter) {
		in = append(in, constant.HjFooter...)
	}
	return &ChjMsgUtils{raw: in}
}

func (o *ChjMsgUtils) ParseChjMsgDataTags() (result *entity.ChjMsgDataTags, exception *entity.ChjMsgDataExceptionInfo) {
	result = new(entity.ChjMsgDataTags)
	var err error
	var tagFieldIndex int
	var key, value string
	var emitTime time.Time
	cpIndex := bytes.Index(o.raw, constant.DataSplit)
	if cpIndex < 0 || len(o.raw) < 12 {
		lotus.Info(fmt.Sprintf("skip -1 index of cp, raw msg: %s", string(o.raw)))
		return
	}
	tagsArea := o.raw[6:cpIndex]
	o.cmd = o.raw[cpIndex+5 : len(o.raw)-8]
	fields := bytes.FieldsFunc(tagsArea, func(r rune) bool {
		return r == ';' || r == ','
	})
	for _, v := range fields {
		tagFieldIndex = bytes.IndexByte(v, '=')
		if tagFieldIndex < 0 {
			lotus.Info(fmt.Sprintf("skip -1 index of = with field %s", string(v)))
			continue
		}
		key, value = string(v[:tagFieldIndex]), string(v[tagFieldIndex+1:])
		switch key {
		case "USAGE":
			result.Usage = value
		case "DEV":
			result.Dev = value
		case "QN":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if emitTime, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.Qn = emitTime.Format("2006-01-02T15:04:05")
		case "ST":
			result.St = value
		case "CN":
			if result.Cn, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
		case "PW":
			result.Pw = value
		case "MN":
			result.Mn = value
		case "Flag":
			if result.Flag, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
		case "PNUM":
			if result.PNum, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
		case "PNO":
			if result.PNo, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
		default:
			if result.Unknown == nil {
				result.Unknown = map[string]string{}
			}
			result.Unknown[key] = value
		}
	}
	return
}
func (o *ChjMsgUtils) ParseChjMsgDataCmdParams() (result *entity.ChjMsgDataCmdParams, exception *entity.ChjMsgDataExceptionInfo) {
	var err error
	var cmdFieldIndex int
	var t time.Time
	result = new(entity.ChjMsgDataCmdParams)
	fields := bytes.FieldsFunc(o.cmd, func(r rune) bool {
		return r == ';' || r == ','
	})
	for _, v := range fields {
		cmdFieldIndex = bytes.IndexByte(v, '=')
		if cmdFieldIndex < 0 {
			lotus.Info(fmt.Sprintf("skip -1 index of = with field %s", string(v)))
			continue
		}
		key, value := string(v[:cmdFieldIndex]), string(v[cmdFieldIndex+1:])
		switch key {
		case "AlarmTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			AlarmTime := t.Format("2006-01-02T15:04:05")
			result.AlarmTime = &AlarmTime
		case "HVER":
			result.HVer = &value
		case "HeartbeatTime":
			var HeartbeatTime int64
			if HeartbeatTime, err = strconv.ParseInt(value, 10, 64); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.HeartbeatTime = &HeartbeatTime
		case "LoginTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			LoginTime := t.Format("2006-01-02T15:04:05")
			result.LoginTime = &LoginTime
		case "PROT":
			result.Prot = &value
		case "SVDATE":
			if len(value) < 10 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("2006-01-02", value[:10], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			SvDate := t.Format("2006-01-02T15:04:05")
			result.SvDate = &SvDate
		case "SVER":
			result.SVer = &value
		case "CstartTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			CstartTime := t.Format("2006-01-02T15:04:05")
			result.CstartTime = &CstartTime
		case "BeginTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			BeginTime := t.Format("2006-01-02T15:04:05")
			result.BeginTime = &BeginTime
		case "EndTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			EndTime := t.Format("2006-01-02T15:04:05")
			result.EndTime = &EndTime
		case "DataTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			DataTime := t.Format("2006-01-02T15:04:05")
			result.DataTime = &DataTime
		case "SystemTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			SystemTime := t.Format("2006-01-02T15:04:05")
			result.SystemTime = &SystemTime
		case "RestartTime":
			if len(value) < 14 {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
				return
			}
			if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			RestartTime := t.Format("2006-01-02T15:04:05")
			result.RestartTime = &RestartTime
		case "PolId":
			result.PolId = &value
		case "QnRtn":
			var QnRtn int
			if QnRtn, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.QnRtn = &QnRtn
		case "ExeRtn":
			var ExeRtn int
			if ExeRtn, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.ExeRtn = &ExeRtn
		case "RtdInterval":
			var RtdInterval int
			if RtdInterval, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.RtdInterval = &RtdInterval
		case "MinInterval":
			var MinInterval int
			if MinInterval, err = strconv.Atoi(value); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.MinInterval = &MinInterval
		case "InfoId":
			result.InfoId = &value
		case "NewPW":
			result.NewPW = &value
		case "OverTime":
			var OverTime int64
			if OverTime, err = strconv.ParseInt(value, 10, 64); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.OverTime = &OverTime
		case "ReCount":
			var ReCount int64
			if ReCount, err = strconv.ParseInt(value, 10, 64); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.ReCount = &ReCount
		case "VaseNo":
			var VaseNo int64
			if VaseNo, err = strconv.ParseInt(value, 10, 64); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.VaseNo = &VaseNo
		case "Stime":
			var Stime int64
			if Stime, err = strconv.ParseInt(value, 10, 64); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.Stime = &Stime
		case "Ctime":
			var Ctime int64
			if Ctime, err = strconv.ParseInt(value, 10, 64); err != nil {
				exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
				return
			}
			result.Ctime = &Ctime
		default:
			switch true {
			case strings.HasSuffix(key, constant.SuffixCheckValue):
				if result.CheckValue == nil {
					result.CheckValue = map[string]*float64{}
				}
				if result.CheckValue[strings.ReplaceAll(key, constant.SuffixCheckValue, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixStandardValue):
				if result.StandardValue == nil {
					result.StandardValue = map[string]*float64{}
				}
				if result.StandardValue[strings.ReplaceAll(key, constant.SuffixStandardValue, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}

			case strings.HasSuffix(key, constant.SuffixRngMin):
				if result.RngMin == nil {
					result.RngMin = map[string]*float64{}
				}
				if result.RngMin[strings.ReplaceAll(key, constant.SuffixRngMin, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixRngMax):
				if result.RngMax == nil {
					result.RngMax = map[string]*float64{}
				}
				if result.RngMax[strings.ReplaceAll(key, constant.SuffixRngMax, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixRtd):
				if result.Rtd == nil {
					result.Rtd = map[string]*float64{}
				}
				if result.Rtd[strings.ReplaceAll(key, constant.SuffixRtd, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixRT):
				if result.RT == nil {
					result.RT = map[string]*float64{}
				}
				if result.RT[strings.ReplaceAll(key, constant.SuffixRT, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixRS):
				if result.RS == nil {
					result.RS = map[string]*float64{}
				}
				if result.RS[strings.ReplaceAll(key, constant.SuffixRS, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixCou):
				if result.Cou == nil {
					result.Cou = map[string]*float64{}
				}
				if result.Cou[strings.ReplaceAll(key, constant.SuffixCou, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixEFlag):
				if result.EFlag == nil {
					result.EFlag = map[string]*string{}
				}
				result.EFlag[strings.ReplaceAll(key, constant.SuffixEFlag, constant.Empty)] = &value
			case strings.HasSuffix(key, constant.SuffixFlag):
				if result.Flag == nil {
					result.Flag = map[string]*string{}
				}
				result.Flag[strings.ReplaceAll(key, constant.SuffixFlag, constant.Empty)] = &value
			case strings.HasSuffix(key, constant.SuffixZsMax):
				if result.ZsMax == nil {
					result.ZsMax = map[string]*float64{}
				}
				if result.ZsMax[strings.ReplaceAll(key, constant.SuffixZsMax, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixZsAvg):
				if result.ZsAvg == nil {
					result.ZsAvg = map[string]*float64{}
				}
				if result.ZsAvg[strings.ReplaceAll(key, constant.SuffixZsAvg, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixZsMin):
				if result.ZsMin == nil {
					result.ZsMin = map[string]*float64{}
				}
				if result.ZsMin[strings.ReplaceAll(key, constant.SuffixZsMin, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixZsRtd):
				if result.ZsRtd == nil {
					result.ZsRtd = map[string]*float64{}
				}
				if result.ZsRtd[strings.ReplaceAll(key, constant.SuffixZsRtd, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixMax):
				if result.Max == nil {
					result.Max = map[string]*float64{}
				}
				if result.Max[strings.ReplaceAll(key, constant.SuffixMax, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixAvg):
				if result.Avg == nil {
					result.Avg = map[string]*float64{}
				}
				if result.Avg[strings.ReplaceAll(key, constant.SuffixAvg, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixMin):
				if result.Min == nil {
					result.Min = map[string]*float64{}
				}
				if result.Min[strings.ReplaceAll(key, constant.SuffixMin, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixData):
				if result.Data == nil {
					result.Data = map[string]*float64{}
				}
				if result.Data[strings.ReplaceAll(key, constant.SuffixData, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixDayData):
				if result.DayData == nil {
					result.DayData = map[string]*float64{}
				}
				if result.DayData[strings.ReplaceAll(key, constant.SuffixDayData, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixNightData):
				if result.NightData == nil {
					result.NightData = map[string]*float64{}
				}
				if result.NightData[strings.ReplaceAll(key, constant.SuffixNightData, constant.Empty)], err = parseFloat(value, 64); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
			case strings.HasSuffix(key, constant.SuffixInfo):
				if result.Info == nil {
					result.Info = map[string]*string{}
				}
				result.Info[strings.ReplaceAll(key, constant.SuffixInfo, constant.Empty)] = &value
			case strings.HasSuffix(key, constant.SuffixSN):
				if result.SN == nil {
					result.SN = map[string]*string{}
				}
				result.SN[strings.ReplaceAll(key, constant.SuffixSN, constant.Empty)] = &value
			case strings.HasSuffix(key, constant.SuffixLog):
				if result.Log == nil {
					result.Log = map[string]*string{}
				}
				result.Log[strings.ReplaceAll(key, constant.SuffixLog, constant.Empty)] = &value
			case strings.HasSuffix(key, constant.SuffixSampleTime):
				if len(value) < 14 {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: fall.WrongDigitOfTime.Error()}
					return
				}
				if t, err = time.ParseInLocation("20060102150405", value[:14], time.Local); err != nil {
					exception = &entity.ChjMsgDataExceptionInfo{Key: key, Message: err.Error()}
					return
				}
				if result.SampleTime == nil {
					result.SampleTime = map[string]string{}
				}
				result.SampleTime[strings.ReplaceAll(key, constant.SuffixSampleTime, constant.Empty)] = t.Format("2006-01-02T15:04:05")
			default:
				if result.Unknown == nil {
					result.Unknown = map[string]*string{}
				}
				result.Unknown[key] = &value
			}
		}
	}
	return
}

func (o *ChjMsgUtils) Parse() (result *entity.ChjMsgData) {
	result = new(entity.ChjMsgData)
	result.Msg = o.msg
	if Tags, err := o.ParseChjMsgDataTags(); err != nil {
		if Tags != nil {
			result.Tags = Tags
		}
		result.ExceptionInfo = err
		return
	} else {
		result.Tags = Tags
	}
	if CmdParams, err := o.ParseChjMsgDataCmdParams(); err != nil {
		if CmdParams != nil {
			result.CmdParams = CmdParams
		}
		result.ExceptionInfo = err
		return
	} else {
		result.CmdParams = CmdParams
	}
	return
}
func (o *ChjMsgUtils) GetParams() map[string]string {
	var result = map[string]string{}
	var cmdFieldIndex int
	var key, value string
	fields := bytes.FieldsFunc(o.cmd, func(r rune) bool {
		return r == ';' || r == ','
	})
	for _, v := range fields {
		cmdFieldIndex = bytes.IndexByte(v, '=')
		if cmdFieldIndex < 0 {
			lotus.Info(fmt.Sprintf("skip -1 index of = with field %s", string(v)))
			continue
		}
		key, value = string(v[:cmdFieldIndex]), string(v[cmdFieldIndex+1:])
		result[key] = value
	}
	return result
}
