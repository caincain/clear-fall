package rule

import (
	"bytes"
	"fmt"
	"gitee.com/caincain/clear-fall/entity"
	"gitee.com/caincain/common-brick/lotus"
	"go.uber.org/zap"
)

func HJ212Response(in *entity.Msg) (out []byte, tags *entity.ChjMsgTags) {
	var err error
	tags = entity.NewChjMsgTags()
	if err = tags.From(in); err != nil {
		fmt.Println(string(in.Content))
		lotus.Warn(err.Error())
		return
	}
	if tags.Mn == "" {
		fmt.Println(string(in.Content))
		lotus.Warn(fmt.Sprintf("mn can not be empty"), zap.Binary("content", in.Content))
		return
	}
	tags.PNum = nil
	tags.PNo = nil
	if tags.Is2017() {
		out = tags.WrapChj2017()
		return
	} else {
		if bytes.Contains(in.Content, []byte("PROT=")) {
			out = tags.WrapCChj2005()
			return
		} else {
			out = tags.WrapChj2005()
			return
		}
	}
}

func HJ212ResponseFromBytes(in []byte) (out []byte, err error) {
	tags := entity.NewChjMsgTags()
	if err = tags.FromBytes(in); err != nil {
		fmt.Println(string(in))
		lotus.Warn(err.Error())
		return
	}
	if tags.Mn == "" {
		fmt.Println(string(in))
		lotus.Warn(fmt.Sprintf("mn can not be empty"), zap.Binary("content", in))
		return
	}
	tags.PNum = nil
	tags.PNo = nil
	if tags.Is2017() {
		out = tags.WrapChj2017()
		return
	} else {
		if bytes.Contains(in, []byte("PROT=")) {
			out = tags.WrapCChj2005()
			return
		} else {
			out = tags.WrapChj2005()
			return
		}
	}
}
