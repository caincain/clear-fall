package clear_fall

import (
	"strconv"
	"unsafe"
)

func CheckSum(data []byte) uint16 {
	var crc16 uint16
	var check uint16
	crc16 = 0xFFFF
	for _, v := range data {
		crc16 = (crc16 >> 8) ^ uint16(v)
		for i := 0; i < 8; i++ {
			check = crc16 & 0x001
			crc16 >>= 1
			if check == 0x001 {
				crc16 ^= 0xA001
			}
		}
	}
	return crc16
}

func Bytes2Uint(in []byte) (out uint64, err error) {
	return strconv.ParseUint(string(in), 10, 64)
}

func StringToBytes(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(
		&struct {
			string
			Cap int
		}{s, len(s)},
	))
}