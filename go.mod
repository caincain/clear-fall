module gitee.com/caincain/clear-fall

go 1.18

require (
	gitee.com/caincain/common-brick v0.0.0-20211122092705-3e68d47618cf
	go.uber.org/zap v1.19.1
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
